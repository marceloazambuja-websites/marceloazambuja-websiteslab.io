---
title: Doçaria Silva - Com açucar e com afeto
type: partner
category: Nosso Apoio para comunidade
website: 'https://www.facebook.com/SilvaDocaria/'
logo: /images/partners/docaria2.jpg
socials: []
---

Tudo feito com carinho e cuidado nos menores detalhes para nossos clientes! Conheça um pouco da Doçaria Silva: [https://www.facebook.com/SilvaDocaria/](https://www.facebook.com/SilvaDocaria/)

![taverna](/images/partners/docaria.jpg)
