---
title: Vídeos

menu:
  main:
    weight: 8

draft: false
---


{{% hero %}}
## Vídeo do Dia 1 (23 abril 2019)
<a class="btn primary" target="_blank" rel="noopener" href="https://www.youtube.com/watch?v=UWzdyROzxVI">Cliqui aqui e veja o YouTube do dia 01</a>

##   
## Vídeo do Dia 2 (24 abril 2019)
<a class="btn primary" target="_blank" rel="noopener" href="https://www.youtube.com/watch?v=JAGSJiwkc58">Cliqui aqui e veja o YouTube do dia 02</a>

##   
## Vídeo do Dia 3 (25 abril 2019)
<a class="btn primary" target="_blank" rel="noopener" href="https://www.youtube.com/watch?v=PsJ620WVANs">Cliqui aqui e veja o YouTube do dia 03</a>

{{% /hero %}}



