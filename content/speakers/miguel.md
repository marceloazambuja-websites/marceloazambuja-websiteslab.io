---
id: miguel
name: Miguel "MW" Wilbert
company: MW Informática
featured: false
photo: /images/speakers/miguel.jpg
photoUrl: /images/speakers/miguel.jpg
socials:
  - icon: youtube
    link: 'https://www.youtube.com/channel/UCbK5Us4E-HsXw6fQ1PYUuog/'
    name: 'Canal MW Informática'

#  - icon: twitter
#    link: 'https://twitter.com/'
#    name: '@'
# icon: linkedin
#    link: 'http://linkedin.com/in/manoela-nascimento-8467a319'
#    name: LinkedIn de Manu Nascimento
shortBio: >-
  YouTuber do Canal MW Informática (+ 80 mil assinantes), onde fala principalmente sobre análises de hardware e peças baratas pra montagem de PCs Gamer, gameplays, unboxing - placas de vídeo, processadores, comparações entre placas diversas, etc.  
  
  Estudante da área de TI (Sistemas de Informação) na Faccat.
companyLogo: /images/speakers/company/mw.jpg
companyLogoUrl: /images/speakers/company/mw.jpg
#country: 'Brasil'

---

  YouTuber do Canal MW Informática (+ 80 mil assinantes), onde fala principalmente sobre análises de hardware e peças baratas pra montagem de PCs Gamer, gameplays, unboxing - placas de vídeo, processadores, comparações entre placas diversas, etc.  
  
  Estudante da área de TI (Sistemas de Informação) na Faccat.
